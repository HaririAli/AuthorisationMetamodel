policy ucon-lifecycle {
    target clause Attributes.session.request.resource.type in Attributes.resourceTypes.continuousServices
    apply firstApplicable
    rule init {
        target clause Attributes.session.phase == "init"
        permit
        on permit {
            obligation initToPre {
                Attributes.session.phase = "pre"
            }
        }
    }
    rule preToOngoing {
        target clause Attributes.session.inProgress
                  and Attributes.session.phase == "pre"
                  and Attributes.session.decision == "permit"
                  and Attributes.session.directives.enforced
        permit
        on permit {
            obligation preToOngoing {
                Attributes.session.phase = "ongoing"
            }
        }
    }
    rule ongoingToOngoing {
        target clause Attributes.session.inProgress
                  and Attributes.session.phase == "ongoing"
                  and Attributes.session.decision == "permit"
                  and Attributes.session.directives.enforced
        permit
    }
    rule ongoingToPost {
        target clause Attributes.session.phase == "ongoing"
                  and (Attributes.session.decision == "deny"
                   and Attributes.session.directives.enforced
                   or !Attributes.session.inProgress)
        permit
        on permit {
            obligation toPost {
                Attributes.session.phase = "post"
            }
        }
    }
    rule default {
        permit
        on permit {
            obligation terminate {
                Attributes.session.phase = "end"
            }
        }
    }
};