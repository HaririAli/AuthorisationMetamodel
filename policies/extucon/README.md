### Policy
The policy specifies rules for a pay as you go service charged by MB of data. The price per 1MB of data is 0.02€. The user must have at least 5€ to start using the service. If the user balance drops below 1€, then their access will be suspended and they will be prompted to top up their balance. This is done to avoid overdraft. We assume that the Policy Enforcement Point (PEP) is charging the user.

#### Rule pre
rule pre specifies that if access has not started yet and balance is equal or greater than 5€, then permit access. It also incorporates an obligation that commands the PEP to configure the charging rate at 0.02€ per MB of data.

#### Rule preLowBalance
rule preLowBalance specifies that if access has not started yet and the balance is below 5€, then deny access and command the PEP to prompt the user to top up their account.

#### Rule ongoing
rule ongoing specifies that as long as the user balance is above 1€, permit access.

#### Rule ongoingLowBalance
rule ongoingLowBalance specifies that if the user balance is below 1€, then deny access.

#### Rule suspendedLowBalance
rule suspendedLowBalance specifies that if the balance is below 1€, then deny access and command the PEP to prompt the user to top up their account.

#### Rule suspendedPaid
rule suspendedPaid specifies that if the user balance is above or equal to 1€, then permit access.

#### Rule post
rule post specifies that if access has ended (session in post phase), command the PEP to print the invoice.

#### Rule defaultDeny
rule defaultDeny is added to have a last resort rule that denies access in case none of the other rules applies.

### Flow
Let us assume that the user requests access with a balance of 3€. The flow will be as follows:
* rule preLowBalance balance applies and the user will be asked to top up their account.
* The user tops up their account with 2€, so rule pre applies and access will be granted.
* Session moves to ongoing phase where rule ongoing applies and user is allowed to keep using the service.
* After consuming 225MB of data, the balance drops to 0.5€, so rule ongoingLowBalance applies and the user access is suspended.
* Session moves to suspended phase where rule suspendedLowBalance applies, so the user is prompted to top their balance up.
  * If the user does not top up their balance, session moves to post phase
  * If the user tops up their balance, rule suspendedPaid applies and access will be permitted again, then session moves to ongoing phase and rule ongoing applies as aforementioned.
* Finally, when the session moves to the post phase (either because user did not top up their balance or because they explicitly stopped their access), rule post applies and the PEP prints an invoice.